<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNstTransformarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nst_transformar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('diagrams');
            $table->string('amps')->nullable();
            $table->text('description')->nullable();
            $table->string('relay')->nullable();
            $table->string('product')->nullable();
            $table->string('customer')->nullable();
            $table->string('name')->nullable();
            $table->string('usage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nst_transformar');
    }
}
