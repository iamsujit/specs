<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/drawing','PagesController@drawing')->name('drawing');
Route::get('/drawing/nwd','PagesController@drawingNwd')->name('drawing.nwd');
Route::get('/drawing/nwd/add','PagesController@drawingNwdAdd')->name('drawing.nwd.add');
Route::get('/drawing/nas','PagesController@drawingNwd')->name('drawing.nas');
Route::get('/drawing/nwd/add','PagesController@drawingNwdAdd')->name('drawing.nwd.add');
