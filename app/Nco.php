<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nco extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nco';
    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
