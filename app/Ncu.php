<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ncu extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ncu';
    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
