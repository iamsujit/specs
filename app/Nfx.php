<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nfx extends Model
{

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nfx';

    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
