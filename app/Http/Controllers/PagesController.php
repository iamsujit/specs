<?php

namespace App\Http\Controllers;

use App\Nas;
use App\Nwd;
use App\Nca;
use App\Ncu;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function drawing()
    {
    	$nca = Nca::all();
    	$nas = Nas::all();
    	$ncu = Ncu::all();
    	$nas_nca = $nas->merge($nca);
    	$diagrams = $nas_nca->merge($ncu);
    	return view('drawing',compact('diagrams'));
    }

    public function drawingNwd()
    {
    	$nwd = Nwd::all();
    	return view('nwd',compact('nwd'));
    }
    public function drawingNwdAdd()
    {
    	$nwd = Nwd::all()->last();
    	$diagram = trim($nwd->diagrams,'NWD');
    	$diagram += (int)$diagram;
    	$diagram = 'NWD000'.$diagram;
    	return view('new',compact('nwd','diagram'));
    }
}
