<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nht extends Model
{

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nht';
    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
