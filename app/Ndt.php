<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ndt extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ndt';
    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
