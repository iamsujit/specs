<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nst extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nst';
    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
