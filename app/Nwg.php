<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nwg extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nwg';
    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
