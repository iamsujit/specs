<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nmd extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nmd';
    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
