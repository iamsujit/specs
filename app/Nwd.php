<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nwd extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nwd';
    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
