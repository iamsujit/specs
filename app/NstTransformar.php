<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NstTransformar extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nst_transformar';
    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
