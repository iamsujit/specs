<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nas extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nas';
    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
