<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nal extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nal';

    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
