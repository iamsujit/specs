<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nmp extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nmp';
    protected $fillable = ['diagrams','volts','description','relay','product','customer','name'];
}
