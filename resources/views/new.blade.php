@extends('partials.layout')
@section('content')
<section class="content">
    <div class="col-md-12" style="padding-top: 3em;">
        <a href="/drawing/nwd">Back</a>
        <div role="dialog">
            <div class="modal-dialog">
                @if ($errors->any())
                <p class="alert alert-success text-center col-md-8 col-md-offset-2" style="margin-top: 30px;z-index: 10;">{{ implode('', $errors->all(':message')) }}</p>
                @endif
                @if (Session::has('message'))
                {!! Session::get('message') !!}
                @endif
                <form method="POST" action="{{ route('drawing.nwd.add') }}">
                    {{ csrf_field() }}
                    <!-- Modal content-->
                    <div class="modal-content no-border-radius">
                        <div class="modal-header" style="border-bottom: 0px;">

                        </div>
                        <div class="modal-body">
                            <div class="container-fluid" style="padding: 0em 2.3em;">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <input type="text" name="diagrams" class="no-border-radius form-control input-large" placeholder="Diagram" required="required" value="{{$diagram}}" disabled>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="volts" class="form-control no-border-radius input-large" placeholder="Volts"  required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <textarea type="text" name="description" class="form-control no-border-radius input-large" placeholder="Description" required="required"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="product" class="form-control no-border-radius input-large" placeholder="Product" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="customer" class="form-control no-border-radius input-large" placeholder="Customer" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="replay" class="form-control no-border-radius input-large" placeholder="Relay" required="required">
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3 text-center" style="margin-top: 3rem;">
                                        <button type="submit" class="btn btn-primary btn-block no-border-radius input-large" style="background-color: #33a0c1; color: #fff;"><span style="font-weight: bold;">Submit</span></button><br>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js-section')

<script type="text/javascript">
    $(document).ready(function(){
        $('#dealersTable').DataTable();
    });
</script>

@endsection
