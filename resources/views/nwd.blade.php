@extends('partials.layout')
@section('content')
<section class="content">
    <div class="col-md-12" style="padding-top: 3em;">
        <a href="/drawing/nwd/add" class="btn btn-primary">Add new</a>
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="dealersTable" cellspacing="0" width="100%">
                <thead style="background-color: #d6d9db;">
                    <tr>
                        <th class="text-center">Sr no.</th>
                        <th class="text-center">Diagram</th>
                        <th class="text-center">Volts</th>
                        <th class="text-center">Description</th>
                        <th class="text-center">Relay</th>
                        <th class="text-center">Product</th>
                        <th class="text-center">Customer</th>
                        <th class="text-center">Name</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($nwd as $diag)
                    <tr class="text-center">
                        <td>{{$diag->id}}</td>
                        <td><a href="{{ route('drawing.nwd.add') }}">{{$diag->diagrams}}</a></td>
                        <td>
                            {{$diag->volts}}
                        </td>
                        <td>
                            {{$diag->description}}
                        </td>
                        <td>{{$diag->relay}}</td>
                        <td>{{$diag->product}}</td>
                        <td>{{$diag->customer}}</td>
                        <td>{{$diag->name}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection
@section('js-section')

<script type="text/javascript">
    $(document).ready(function(){
        $('#dealersTable').DataTable();
    });
</script>

@endsection
