@extends('partials.layout')
@section('content')
<section class="content">
    <div class="col-md-12" style="padding-top: 3em;">
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="dealersTable" cellspacing="0" width="100%">
                <thead style="background-color: #d6d9db;">
                    <tr>
                        <th class="text-center">Sr no.</th>
                        <th class="text-center">Diagrams</th>
                        <th class="text-center">Description</th>
                        <th class="text-center">Relay</th>
                        <th class="text-center">Product</th>
                        <th class="text-center">Customer</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($diagrams as $diagram)
                    <tr class="text-center">
                        <td>{{$diagram->id}}</td>
                        <td><a href="#">{{$diagram->diagrams}}</a></td>
                        <td>
							{{$diagram->description}}
                        </td>
                        <td>
                            {{$diagram->relay}}
                        </td>
                        <td>{{$diagram->product}}</td>
                        <td>{{$diagram->customer}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
@endsection
@section('js-section')

<script type="text/javascript">
    $(document).ready(function(){
        $('#dealersTable').DataTable();
    });
</script>

@endsection
